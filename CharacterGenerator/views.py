from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseForbidden
from django.template import loader

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

from random import randint
from kv_pathfinder import settings
from . import const

from CharacterGenerator.forms import CreationForm, Sign_up, Item_form, Account_form, Edit_form
from .models import Alignments, Race, Character, Class, Racial_traits, Skills, \
    Char_skills, Features, Feat_prerequisites, Char_features, Gods, Schools, Items, \
    Equipment, Spells, Char_spells, Special_spells, Class_feats, Char_class_feats,  \
    Adventures, Char_adventures


def change_data(request):
    if request.user.is_authenticated:
        user = request.user

        if request.method == 'POST':
            data_form = Account_form(request.POST)
            if data_form.is_valid():
                user.first_name = data_form.cleaned_data["fname"]
                user.last_name = data_form.cleaned_data["lname"]
                user.save()

                return redirect('account')
            else:
                data_form = Account_form()
        else:
            data_form = Account_form()
            template = loader.get_template('change_acc.html')
            context = {'form': data_form}

            return HttpResponse(template.render(context, request))
    else:
        return redirect('/accounts/login')


def change_password(request):
    if request.user.is_authenticated:
        return redirect('password_change')


def account(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    else:
        user = request.user

    template = loader.get_template('account.html')
    context = {'user': user}
    return HttpResponse(template.render(context, request))


def delete_account(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    else:
        user = request.user

    user.delete()
    return redirect('/accounts/login')


def char_delete(request, char_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    char.delete()

    return redirect('chars')


def char_edit(request, char_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    if request.method == 'POST':
        data_form = Edit_form(request.POST)
        if data_form.is_valid():
            if data_form.cleaned_data["name"] is not None and data_form.cleaned_data["name"] != "":
                char.name = data_form.cleaned_data["name"]

            if data_form.cleaned_data["xp"] is not None:
                char.xp += data_form.cleaned_data["xp"]

            if data_form.cleaned_data["cp"] is not None:
                char.cp += data_form.cleaned_data["cp"]

            char.save()
            return redirect('char', char_id)
        else:
            data_form = Edit_form()
    else:
        data_form = Edit_form()
        template = loader.get_template('edit_char.html')
        context = {'form': data_form, 'char': char}
        return HttpResponse(template.render(context, request))


def main(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')

    template = loader.get_template('main.html')
    context = {}
    return HttpResponse(template.render(context, request))


def align(request, align_id):
    try:
        align = Alignments.objects.get(pk=align_id)
    except ObjectDoesNotExist:
        return HttpResponse(const.err_nexist.format("Alignment"))

    return HttpResponse(str(align))


def race_info(request, race_id):
    # TODO exception
    race = Race.objects.get(pk=race_id)
    return HttpResponse(str(race))


def characters(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)
    else:
        user = request.user

    chars = Character.objects.filter(user_id=user.pk)
    template = loader.get_template('chars.html')
    context = {'chars': chars}
    return HttpResponse(template.render(context, request))


def character(request, char_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)
    else:
        user = request.user

    # mēģina atrast objektu
    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    char_skills = Char_skills.objects.filter(char_id=char)
    for c_skill in char_skills:
        if c_skill.skill_id.trained and c_skill.ranks == 0:
            c_skill.print_modifier = ""
            c_skill.total = ""
            c_skill.misc = ""
        else:
            c_skill.print_modifier = char.skill_mods(c_skill.skill_id.modifier)
            c_skill.total = c_skill.ranks + c_skill.print_modifier + c_skill.misc

    # pārreķīna saves
    char.total_fort_save = char.fort_save + char.CON()
    char.total_ref_save = char.ref_save + char.DEX()
    char.total_will_save = char.will_save + char.WIS()

    char_feat = Char_features.objects.filter(char_id=char)

    char_class_feat = Char_class_feats.objects.filter(char_id=char)
    char_class_feat = list(char_class_feat)

    # atrast initiativi
    char.improved_initiative = 0
    ii = Features.objects.get(name="Improved Initiative")

    try:
        tmp = Char_features.objects.get(char_id=char, feat_id=ii)
        char.improved_initiative = 4
    except ObjectDoesNotExist:
        pass

    char.total_initiative = char.improved_initiative + char.DEX()

    # melee/ranged attack
    char.total_melee = char.atk_bonus + char.STR()
    char.total_ranged = char.atk_bonus + char.DEX()

    if char.god is not None:
        char_class_feat.append("God: " + char.god.name)
        char_class_feat.append("Holy weapon: " + char.god.holy_weapon)

    template = loader.get_template('char.html')
    context = {
        'char': char, 'c_skills': char_skills, 'feats': char_feat, 'class_feats': char_class_feat
    }

    return HttpResponse(template.render(context, request))


def adventures(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)

    advs = Adventures.objects.all()

    template = loader.get_template('adventures.html')
    context = {'adventures': advs}
    return HttpResponse(template.render(context, request))


def adventure(request, adv_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)
    else:
        user = request.user

    adv = Adventures.objects.get(pk=adv_id)
    c_adventure = Char_adventures.objects.filter(adventure=adv_id)
    chars = Character.objects.filter(user_id=user.pk)

    template = loader.get_template('adventure.html')
    context = {'c_adventure': c_adventure, 'adventure': adv, 'chars': chars}
    return HttpResponse(template.render(context, request))


def create_adventure(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)

    if request.method == 'POST':
        if request.POST["name"] != "" and len(request.POST["name"]) <= 40:
            adv = Adventures.objects.create()
            adv.name = request.POST["name"]
            adv.desc = request.POST["desc"]
            adv.save()
            return redirect('adventure', adv.pk)
        else:
            messages.error(request, "Name required, ensure it has at most 50 symbols")
            return redirect('create_adventure')
    else:
        template = loader.get_template('adv_new.html')
        context = {}
        return HttpResponse(template.render(context, request))


def join_adventure(request, adv_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)
    else:
        user = request.user

    if request.method == 'POST':
        try:
            char = Character.objects.get(pk=int(request.POST["character"]))
        except ObjectDoesNotExist:
            return HttpResponseForbidden(const.err_nexist.format("Character"))

        if char.user_id != user.pk and not user.is_superuser:
            return HttpResponseForbidden(const.err_access)

        try:
            adv = Adventures.objects.get(pk=adv_id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden(const.err_nexist.format("Adventure"))

        c_adv, created = Char_adventures.objects.get_or_create(adventure=adv, char_id=char)
        if created:
            c_adv.save()
        else:
            messages.error(request, "Character already joined")

        return redirect('adventure', adv_id)


def adv_results(request, adv_id, char_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    try:
        adv = Adventures.objects.get(pk=adv_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Adventure"))

    if request.method == 'POST':
        try:
            c_adv = Char_adventures.objects.get(adventure=adv, char_id=char)
        except ObjectDoesNotExist:
            return HttpResponseForbidden("Character have not joined adventure")

        c_adv.notes += request.POST["notes"]
        if request.POST["kills"] != '' and int(request.POST["kills"]) >= 0:
            c_adv.kills += int(request.POST["kills"])
        else:
            messages.error(request, "Monsters killed should be at least 0")
            return redirect('adv_results', adv_id=adv_id, char_id=char_id)

        if request.POST["xp"] != '' and int(request.POST["xp"]) >= 0:
            c_adv.xp = int(request.POST["xp"])
            char.xp += int(request.POST["xp"])
            char.save()
        else:
            messages.error(request, "Xp should be at least 0")
            return redirect('adv_results', adv_id=adv_id, char_id=char_id)

        c_adv.save()
        return redirect('adventure', adv_id=adv_id)

    else:
        template = loader.get_template('adv_results.html')
        context = {'adventure': adv, 'char': char}
        return HttpResponse(template.render(context, request))


def create1(request, validation=None):
    if not request.user.is_authenticated:
        return redirect('/accounts/login?next=%s' % request.path)
    else:
        user = request.user

    races = Race.objects.all()
    aligns = Alignments.objects.all()
    classes = Class.objects.all()
    schools = Schools.objects.all()
    gods = Gods.objects.all()

    items = []
    # dabūjam vajadzīgus objektus no db
    for wiz_item in const.wiz_weapons:
        try:
            res = Items.objects.get(name=wiz_item)
            items.append(res)
            print(res)
        except ObjectDoesNotExist:
           # TODO !!
            pass

        print(items)
    template = loader.get_template('create.html')
    context = {'races': races, 'aligns': aligns, 'classes': classes, 'validation': validation, 'schools': schools,
               'gods': gods, 'items': items}
    return HttpResponse(template.render(context, request))


def create2(request, char_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponse(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    c_class = Class.objects.get(pk=char.c_class.pk)
    c_skills = Char_skills.objects.filter(char_id=char)
    char.const_sr = char.skill_ranks
    # pievieno jaunus laukus objektam, kas vajadzīgi attēlošanai
    for c_skill in c_skills:
        c_skill.print_modifier = char.skill_mods(c_skill.skill_id.modifier)
        c_skill.total = c_skill.ranks + c_skill.print_modifier + c_skill.misc
        c_skill.is_trained = c_skill.skill_id.trained

    feats = Features.objects.all()

    for feat in feats:
        feat.req = feat.get_req()

    template = loader.get_template('create2.html')
    context = {'char': char, 'c_skills': c_skills, 'class': c_class, 'feats': feats}
    return HttpResponse(template.render(context, request))


def equipment(request, char_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    items = Equipment.objects.filter(char_id=char)

    template = loader.get_template('equipment.html')
    context = {'char': char, 'items': items}
    return HttpResponse(template.render(context, request))


def get_items(request, char_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponse(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponse(const.err_access)

    items = Items.objects.all()

    for item in items:
        #pievieno priekšmetus, kurus drīkst nopirkt, priekšmetu kategorijas
        item.can_buy = not item.masterwork and (item.magical is None or item.magical == 0)

    template = loader.get_template('get_items.html')
    context = {'char': char, 'items': items}
    return HttpResponse(template.render(context, request))


def create_items(request, char_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    data_form = Item_form(request.POST)

    items = Items.objects.all()

    template = loader.get_template('create_item.html')
    context = {'char': char, 'items': items, 'form': data_form}
    return HttpResponse(template.render(context, request))


def create_item(request, char_id):
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return redirect('accounts/login')
        else:
            user = request.user

        try:
            char = Character.objects.get(pk=char_id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden(const.err_nexist.format("Character"))

        if char.user_id != user.pk and not user.is_superuser:
            return HttpResponseForbidden(const.err_access)

        pass


def add_feat(request, char_id, feat_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    try:
        feat = Features.objects.get(pk=feat_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Feature"))

    no_req = False
    # meklē prasības
    try:
        feat_r = Feat_prerequisites.objects.get(feat_id=feat_id)
    except ObjectDoesNotExist:
        no_req = True

    if no_req:
        if char.avb_feats > 0:
            c_feat, created = Char_features.objects.get_or_create(char_id=char, feat_id=feat)
            # dažus var pievienot vairākas reizes
            if created or feat.multiple:
                char.avb_feats -= 1
                char.save()
                c_feat.save()
            else:
                return HttpResponseForbidden('Feature already acquired')
        else:
            messages.error(request, const.err_cant_add.format(feat.name))
    else:
        if char.avb_feats > 0:
            c_feats = Char_features.objects.filter(char_id=char)

            ok = False
            if feat_r.req_feat == None:
                ok = True
            else:
                for f in c_feats:
                    if f.feat_id == feat_r.req_feat:
                        ok = True
            # pārbauda prasības

            if feat_r.atr_name != None and feat_r.atr_name != const.none:
                ok = ok and (char.get_atr(feat_r.atr_name) >= feat_r.atr_quantity)

            if feat_r.cleric:
                if char.c_class.name != const.cleric:
                    ok = False

            if feat_r.wizard:
                if char.c_class.name != const.wizard:
                    ok = False

            if feat_r.atk_bonus > 0:
                if (char.atk_bonus < feat_r.atk_bonus):
                    ok = False

            if ok:
                c_feat, created = Char_features.objects.get_or_create(char_id=char, feat_id=feat)
                if created or feat.multiple:
                    char.avb_feats -= 1
                    char.save()
                    c_feat.save()
                else:
                    return HttpResponseForbidden('Feature already acquired')
            else:
                messages.error(request, const.err_cant_add.format(feat.name))
        else:
            messages.error(request, const.err_cant_add.format(feat.name))
    return redirect('create2', char_id=char_id)


def train(request, char_id, skill_id):
    if request.method != "POST":
        return HttpResponseForbidden("Here we go again")

    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    try:
        c_skill = Char_skills.objects.get(pk=skill_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Specified skill"))

    if char.skill_ranks != 0:
        if (c_skill.ranks + 1) <= char.level:
            c_skill.ranks += 1
            char.skill_ranks -= 1
            if c_skill.is_class and c_skill.ranks == 1:
                c_skill.misc += 3
            c_skill.save()

            char.save()
        else:
            messages.error(request, const.err_cant_add.format(c_skill.skill_id.name))
    else:
        messages.error(request, const.err_cant_add.format(c_skill.skill_id.name))
        #return HttpResponseForbidden(const.err_cant_add.format(c_skill.skill_id.name))
    return redirect('create2', char_id=char_id)


def buy(request, char_id, item_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk and not user.is_superuser:
        return HttpResponseForbidden(const.err_access)

    try:
        item = Items.objects.get(pk=item_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Item"))

    if "amount" not in request.POST or request.POST["amount"] == '':
        amount = 1
    else:
        try:
            int(request.POST["amount"])
        except TypeError:
            return HttpResponseForbidden('Amount should be a number')

        if int(request.POST["amount"]) > 0:
            amount = int(request.POST["amount"])
        else:
            return HttpResponseForbidden('Incorrect amount')
    if char.cp - (item.price * amount) >= 0:
        char.cp -= item.price * amount

        eq, created = Equipment.objects.get_or_create(char_id=char, item_id=item)

        if created:
            eq.amount = amount
        else:
            eq.amount += amount

        char.save()
        eq.save()

    return redirect('get_items', char_id=char_id)


# pārbauda vai personāža līmenis atbilst prasībam
def check_level(clvl):
    if clvl == 5:
        return 3
    elif clvl >= 3:
        return 2
    else:
        return 1


def add_spells(request, char_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponse(const.err_nexist.format("Character"))
    if char.user_id != user.pk:
        return HttpResponseForbidden(const.err_access)

    if char.c_class.name != const.wizard:
        return redirect('char', char_id=char_id)
    else:
        spells = Spells.objects.all()

    for s in spells:
        # atlasa spējas atbilstoši personāža līmemim un klasei
        s.can_show = False
        s.can_show = not s.unique and s.level <= check_level(char.level) and \
                     (s.wizard and char.c_class.name == const.wizard) and not s.cantrip

    context = {'char': char, 'spells': spells}
    template = loader.get_template('add_spells.html')

    return HttpResponse(template.render(context, request))


def add_spell(request, char_id, spell_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk:
        return HttpResponseForbidden(const.err_access)

    if char.c_class.name != const.wizard:
        return HttpResponseForbidden(const.err_cant_add.format("spell"))
    else:
        try:
            spell = Spells.objects.get(pk=spell_id)
        except ObjectDoesNotExist:
            return HttpResponseForbidden(const.err_nexist.format("Spell"))
        # TODO check class
        if char.max_spells > 0:
            if check_level(char.level) <= spell.level:
                c_spell, created = Char_spells.objects.get_or_create(char_id=char, spell_id=spell)

                if created:
                    char.max_spells -= 1
                    c_spell.uses = spell.uses + (char.skill_mods(spell.mod) if spell.mod else 0)
                    char.save()
                    c_spell.save()
                else:
                    messages.error(request, const.err_cant_add.format(spell.name) + " Spell already acquired.")
            else:
                return HttpResponseForbidden(const.err_cant_add.format("spell"))
        else:
            messages.error(request, const.err_cant_add.format(spell.name) + " Spellbook filled.")

    return redirect('spells', char_id=char_id)


def spellbook(request, char_id):
    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk:
        return HttpResponseForbidden(const.err_access)

    if char.c_class.name == const.wizard or char.c_class.name == const.cleric:
        sp_book = Char_spells.objects.filter(char_id=char_id)
    else:
        return HttpResponseForbidden(const.err_nexist.format("Character spellbook"))

    template = loader.get_template('spellbook.html')
    context = {'spellbook': sp_book, 'char': char}

    return HttpResponse(template.render(context, request))


# personāža līmeņa paaugstīnāšas funkcija
def level_up(request, char_id):
    def _get_level(xp):
        if xp >= 15000:
            return 5
        elif xp >= 9000:
            return 4
        elif xp >= 5000:
            return 3
        elif xp >= 2000:
            return 2
        else:
            return 1

    def _level_up_cleric(char, level):
        if level == 5:
            level_up_notes = '''Your channel energy ability increases by +1d6!
                You can prepare one 3rd-level cleric spell per day from the spells on this page! if your
                Wisdom ability score is 16 or higher, you can prepare an extra 3rd-level cleric spell each day!
                Just as you can always swap a prepared 1st-level spell for cure light wounds, you can
                always swap a prepared 3rd-level spell for cure serious wounds!'''

        elif level == 4:
            char.atk_bonus += 1
            char.fort_save += 1
            char.will_save += 1
            level_up_notes = '''
                You can prepare another 1st-level cleric spell each day!
                You can prepare another 2nd-level cleric spell each day!
                Add +1 to your channel energy dC to harm undead!'''

        elif level == 3:
            char.atk_bonus += 1
            char.ref_save += 1
            level_up_notes = '''
                Your channel energy ability increases by +1d6!
                You can prepare one 2nd-level cleric spell per day from the spells on this page! if your Wisdom
                ability score is 14 or higher, you can prepare an extra 2nd-level cleric spell each day!
                Just as you can always swap a prepared 1st-level spell for cure light wounds, you can always
                swap a prepared 2nd-level spell for cure moderate wounds!'''

        elif level == 2:
            char.atk_bonus += 1
            char.fort_save += 1
            char.will_save += 1
            level_up_notes = "You can prepare another 1st-level cleric spell each day!"
        else:
            level_up_notes = "Can not level up"

        return level_up_notes

    def _level_up_fighter(char, level):
        if level == 5:
            char.atk_bonus += 1
            level_up_notes = ""

        elif level == 4:
            char.atk_bonus += 1
            char.fort_save += 1
            level_up_notes = ""

        elif level == 3:
            char.atk_bonus += 1
            char.ref_save += 1
            char.will_save += 1
            cl_feat  = Class_feats.objects.get(name="Armor training")
            feat, created = Char_class_feats.objects.get_or_create(c_feat_id=cl_feat, char_id=char)
            if created:
                feat.save()
                level_up_notes = "New class feature!"

        elif level == 2:
            char.atk_bonus += 1
            char.fort_save += 1
            cl_feat  = Class_feats.objects.get(name="Bravery")
            feat, created = Char_class_feats.objects.get_or_create(c_feat_id=cl_feat, char_id=char)
            if created:
                feat.save()
                level_up_notes = "New class feature!"

        else:
            level_up_notes = "Can not level up"

        return level_up_notes

    def _level_up_rogue(char, level):
        if level == 5:
            level_up_notes = "Your sneak attack increases by +1d6!"

        elif level == 4:
            char.atk_bonus += 1
            char.ref_save += 1
            cl_feat  = Class_feats.objects.get(name="Uncanny dodge")
            feat, created = Char_class_feats.objects.get_or_create(c_feat_id=cl_feat, char_id=char)
            if created:
                feat.save()
                level_up_notes = "Your trapfinding ability increases to +2!\
                                  New class feature!"

        elif level == 3:
            char.atk_bonus += 1
            char.fort_save += 1
            char.will_save += 1
            cl_feat = Class_feats.objects.get(name="Trap sense")
            feat, created = Char_class_feats.objects.get_or_create(c_feat_id=cl_feat, char_id=char)
            if created:
                feat.save()
                level_up_notes = "Your sneak attack increases by +1d6!\
                                  New class feature!"

        elif level == 2:
            char.atk_bonus += 1
            char.ref_save += 1
            cl_feat = Class_feats.objects.get(name="Evasion")
            feat, created = Char_class_feats.objects.get_or_create(c_feat_id=cl_feat, char_id=char)
            if created:
                feat.save()
                level_up_notes = "Your sneak attack increases by +1d6!\
                                  New class feature!"
        else:
            level_up_notes = "Can not level up"

        return level_up_notes

    def _level_up_wizard(char, level):
        if level == 5:
            level_up_notes = '''Add two new 1st-, 2nd-, or 3rd-level wizard spells to your spellbook!
                You can prepare one 3rd-level wizard spell (from the list on this page) per day from
                your spellbook! if your intelligence ability score is 16 or higher, you can prepare an extra
                3rd-level wizard spell each day! Evokers can cast fireball once per day without having to
                prepare it; illusionists can do this with displacement'''

        elif level == 4:
            char.atk_bonus += 1
            char.will_save += 1
            level_up_notes = '''Add two new wizard spells to your spellbook!
                You can prepare another 1st-level wizard spell each day
                You can prepare another 2nd-level wizard spell each day!'''

        elif level == 3:
            char.fort_save += 1
            char.ref_save += 1
            level_up_notes = '''Add two new 1st- or 2nd-level wizard spells to your spellbook!
                You can prepare one 2nd-level wizard spell (from the list on this page) per day from
                your spellbook! if your intelligence ability score is 14 or higher, you can prepare an extra
                2nd-level wizard spell each day! Evokers can cast scorching ray once per day without
                having to prepare it; illusionists can do this with invisibility. '''

        elif level == 2:
            char.atk_bonus += 1
            char.will_save += 1
            level_up_notes = "Add two new 1st-level wizard spells to your spellbook!\
                You can prepare another 1st-level wizard spell each day!"
        else:
            level_up_notes = "Can not level up"

        return level_up_notes

    if not request.user.is_authenticated:
        return redirect('accounts/login')
    else:
        user = request.user

    try:
        char = Character.objects.get(pk=char_id)
    except ObjectDoesNotExist:
        return HttpResponseForbidden(const.err_nexist.format("Character"))

    if char.user_id != user.pk:
        return HttpResponseForbidden(const.err_access)

    lvl_notes = "Can not level up"
    if _get_level(char.xp) >= (char.level + 1):
        hp_inc = randint(1, char.c_class.hit_dice) + char.CON()
        char.hp += hp_inc if hp_inc > 0 else 1
        char.skill_ranks += char.get_ranks()
        if _get_level(char.level) == 3 or _get_level(char.level) == 5:
            char.avb_feats += 1

        if char.c_class.name == const.cleric:
            lvl_notes = _level_up_cleric(char, char.level + 1)
        elif char.c_class.name == const.fighter:
            lvl_notes = _level_up_fighter(char, char.level + 1)
        elif char.c_class.name == const.rogue:
            lvl_notes =_level_up_rogue(char, char.level + 1)
        else:
            lvl_notes =_level_up_wizard(char, char.level + 1)

        char.level += 1
        char.save()
    else:
        messages.error(request, lvl_notes)
        return redirect('chars')

    messages.info(request, lvl_notes)
    return redirect('create2', char_id)


def my_chars(request):
    if request.user.is_authenticated:
        chars = Character.objects.filter(user=request.user)
        context = {'chars': chars}
        template = loader.get_template('chars.html')
        return HttpResponse(template.render(context, request))
    else:
        return redirect('accounts/login')


def create_char(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            data_form = CreationForm(request.POST)

            if data_form.is_valid():
                race_pk = data_form.cleaned_data["race"]
                align_pk = data_form.cleaned_data["align"]
                class_pk = data_form.cleaned_data["c_class"]

                c = Character(name=data_form.cleaned_data["name"], gender=data_form.cleaned_data["gender"])
                c.user_id = request.user.pk
                c.race = Race.objects.get(pk=race_pk)
                c.alignment = Alignments.objects.get(pk=align_pk)
                c.c_class = Class.objects.get(pk=class_pk)

                # noklusēti - pirmais līmenis, pēc izveidošanas var izmainīt
                c.level = 1
                c.xp = data_form.cleaned_data["xp"]

                # atribūtu vērtību pievienošana
                c.strength = data_form.cleaned_data["str"]
                c.dexterity = data_form.cleaned_data["dex"]
                c.constitution = data_form.cleaned_data["con"]
                c.intelligence = data_form.cleaned_data["int"]
                c.wisdom = data_form.cleaned_data["wis"]
                c.charisma = data_form.cleaned_data["cha"]

                if c.race.name == const.elf:
                    c.dexterity += 2
                    c.constitution -= 2
                    c.intelligence += 2
                elif c.race.name == const.dwarf:
                    c.constitution += 2
                    c.wisdom += 2
                    c.charisma -= 2

                c.atk_bonus += c.c_class.atk_bonus
                c.fort_save += c.c_class.fort_save
                c.ref_save += c.c_class.ref_save
                c.will_save += c.c_class.will_save

                c.save()
                c.skill_ranks = c.get_ranks()

                skills = Skills.objects.all()

                for sk in skills:
                    c_sk = Char_skills(char_id=c, skill_id=sk)
                    if sk.name == "Perception" and c.race.name == const.elf:
                        c_sk.misc += 2
                    if ((sk.fighter and c.c_class.name == const.fighter) or
                            (sk.cleric and c.c_class.name == const.cleric) or
                            (sk.rogue and c.c_class.name == const.rogue) or
                            (sk.wizard and c.c_class.name == const.wizard)):
                        c_sk.is_class = True
                    c_sk.save()

                # klases iezīmes
                class_feats = Class_feats.objects.all()

                if c.c_class.name == const.cleric:
                    for cl_feat in class_feats:
                        if cl_feat.name == "Channel energy":
                            char_cl_feat = Char_class_feats.objects.create(c_feat_id=cl_feat, char_id=c)
                            char_cl_feat.save()
                elif c.c_class.name == const.rogue:
                    for cl_feat in class_feats:
                        if cl_feat.name == "Sneak attack":
                            char_cl_feat = Char_class_feats.objects.create(c_feat_id=cl_feat, char_id=c)
                            char_cl_feat.save()
                        if cl_feat.name == "Trapfinding":
                            char_cl_feat = Char_class_feats.objects.create(c_feat_id=cl_feat, char_id=c)
                            char_cl_feat.save()
                elif c.c_class.name == const.wizard:
                    for cl_feat in class_feats:
                        if cl_feat.name == "Arcane bond":
                            char_cl_feat = Char_class_feats.objects.create(c_feat_id=cl_feat, char_id=c)
                            char_cl_feat.save()

                # izveidošanas beigas rogue un fighter klasem

                if c.c_class.name == const.wizard:
                    c.max_spells = c.c_class.max_spells + c.INT()
                    c.school = Schools.objects.get(pk=data_form.cleaned_data["school"])
                    mweapon = Items.objects.get(pk=data_form.cleaned_data["mwp"])
                    spells = Spells.objects.all()
                    s_spells = Special_spells.objects.all()
                    for s in s_spells:
                        if s.school_id == c.school:
                            c_spell = Char_spells.objects.create(char_id=c, spell_id=s,
                                                                 uses=s.spell_id.uses + (
                                                                    c.skill_mods(s.spell_id.mod) if s.spell_id.mod
                                                                    else 0))
                            c_spell.save()

                    for s in spells:
                        if s.cantrip and s.wizard:
                            c_spell = Char_spells.objects.create(char_id=c, spell_id=s, uses=None)
                            c_spell.save()

                    c_items = Equipment.objects.create(char_id=c, item_id=mweapon)
                    c_items.save()

                elif c.c_class.name == const.cleric:
                    c.max_spells = c.c_class.max_spells + (1 if c.wisdom >= 12 else 0)
                    c.god = Gods.objects.get(pk=data_form.cleaned_data["god"])
                    spells = Spells.objects.all()
                    s_spells = Special_spells.objects.all()
                    for s in s_spells:
                        if s.god_id == c.god:
                            c_spell = Char_spells.objects.create(char_id=c, spell_id=s.spell_id,
                                                                 uses=s.spell_id.uses + (
                                                                     c.skill_mods(s.spell_id.mod) if s.spell_id.mod
                                                                     else 0))
                            c_spell.save()

                    for s in spells:
                        if s.cleric:
                            c_spell = Char_spells.objects.create(char_id=c, spell_id=s, uses=None)
                            c_spell.save()

                c.save()
                return redirect('create2', char_id=c.pk)
            # data form not valid
            else:
                data_form = CreationForm()
        else:
            return HttpResponseForbidden(const.err_access)
    else:
        return redirect('accounts/login')
    return create1(request, data_form)


def registration(request):
    if request.method == 'POST':
        form = Sign_up(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('main')
    else:
        form = Sign_up()
    return render(request, "registration/register.html", {'form': form})


def info(request):
    races = Race.objects.all()
    aligns = Alignments.objects.all()
    classes = Class.objects.all()
    template = loader.get_template('info.html')
    context = {'races': races, 'aligns': aligns, 'classes': classes}
    return HttpResponse(template.render(context, request))


def json(request, id):
    race = Race.objects.get(pk=id)
    speed = race.speed
    desc = race.short_desc
    traits = Racial_traits.objects.filter(race_id=race)
    s = []
    for t in traits:
        s.append(t.name)
    answer = {
        'desc': desc,
        'traits': s,
        'speed': speed
    }

    return JsonResponse(answer)
