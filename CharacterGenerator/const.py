none = "None"
#$ git ls-files | grep "\(.html\|.py\)$" | xargs wc -l

#consts
human = "Human"
elf = "Elf"
dwarf = "Dwarf"

fighter = "Fighter"
cleric = "Cleric"
rogue = "Rogue"
wizard = "Wizard"

wiz_weapons = ["Masterwork dagger", "Masterwork quarterstaff", "Masterwork wand", "Masterwork ring"]
#errors

err_access = "You do not have permission to access this page"
err_cant_add = "Can not add {}."
err_nexist = "{} does not exist."
