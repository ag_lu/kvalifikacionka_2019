
from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main'),
    path('register', views.registration, name='registration'),
    path('account', views.account, name='account'),
    path('change_password', views.change_password, name='password'),
    path('change_data', views.change_data, name='edit_acc'),
    path('align/<int:align_id>', views.align, name='align'),
    path('race/<int:race_id>', views.race_info, name='race'),
    path('chars', views.my_chars, name='chars'),
    path('char/<int:char_id>', views.character, name='char'),
    path('create', views.create1, name='create'),
    path('create_char', views.create_char, name='set_character_data'),
    path('info', views.info, name='view info'),
    path('race_traits/<int:id>', views.json, name='race_traits'),
    path('create2/<int:char_id>', views.create2, name='create2'),
    path('train/<int:char_id>/<int:skill_id>', views.train, name='train'),
    path('add/<int:char_id>/<int:feat_id>', views.add_feat, name='add_feat'),
    path('get_items/<int:char_id>', views.get_items, name='get_items'),
    path('buy/<int:char_id>/<int:item_id>', views.buy, name='buy'),
    path('add_spells/<int:char_id>', views.add_spells, name='spells'),
    path('add_spell/<int:char_id>/<int:spell_id>', views.add_spell, name='add_spell'),
    path('create_item/<int:char_id>', views.create_items, name='create_items'),
    path('char/<int:char_id>/level_up', views.level_up, name='level_up'),
    path('adventures', views.adventures, name='adventures'),
    path('adventure/<int:adv_id>', views.adventure, name='adventure'),
    path('char/<int:char_id>/inventory', views.equipment, name='equipment'),
    path('create_adventure', views.create_adventure, name='create_adventure'),
    path('adventure/<int:adv_id>/join_adventure', views.join_adventure, name='join_adventure'),
    path('adv_results/<int:adv_id>/<int:char_id>', views.adv_results, name='adv_results'),
    path('spellbook/<int:char_id>', views.spellbook, name='spellbook'),
    path('delete_char/<int:char_id>', views.char_delete, name='delete_char'),
    path('delete_user', views.delete_account, name='delete_user'),
    path('edit_char/<int:char_id>', views.char_edit, name='char_edit')
]