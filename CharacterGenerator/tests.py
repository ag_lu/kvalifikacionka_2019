from django.test import TestCase, Client
from django.contrib.auth.models import User
from .models import Character, Class, Race, Skills, Schools, Gods, Spells, Alignments, \
    Char_skills, Features, Feat_prerequisites, Char_features


# Create your tests here.
class TestCharacter(TestCase):
    def setUp(self):
        self.password = "348test123"
        self.test_user = User.objects.create_user(username="Tasty", password=self.password)
        self.name = self.test_user.username

        self.align_bad = Alignments.objects.create(name="BAD", code="BB", desc="bad guy")
        self.align_good = Alignments.objects.create(name="GOOD", code="GD", desc="good guy")

        self.race_one = Race.objects.create(name="race1", desc="desc", speed=1, short_desc="test")

        self.class_one = Class.objects.create(name="classy", desc="test_class", hp=4, hit_dice=4, ref_save=0, fort_save=0, will_save=0, skill_rank=2, atk_bonus=1, cp=1000)

        self.skill_one = Skills.objects.create(name="skill", modifier="DEX", desc="short")

    def test_char_create(self):
        cl = Client()

        result = cl.get('/create')
        self.assertRedirects(result, '/accounts/login?next=/create', status_code=302, target_status_code=301)

        cl.login(username=self.name, password=self.password)

        result = cl.get('/create')

        self.assertEqual(result.status_code, 200)

        post_data = {
            'name': '',
            'align': 1,
            'gender': 'test',
            'race': 2,
            'c_class': 1,
            'xp': 0,
            'str': 3,
            'dex': 5,
            'con': 2,
            'int': 4,
            'wis': 6,
            'cha': 4
        }

        result = cl.post('/create_char', post_data)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(Character.objects.count(), 0)

        post_data["name"] = "testertetetetetetetetetetetetetetetetetetetetetete"
        result = cl.post('/create_char', post_data)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(Character.objects.count(), 0)

        post_data["name"] = "normal_name"
        result = cl.post('/create_char', post_data)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(Character.objects.count(), 0)

        post_data["con"] = 10
        result = cl.post('/create_char', post_data)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(Character.objects.count(), 0)

        post_data["race"] = 1
        result = cl.post('/create_char', post_data)
        self.assertEqual(result.status_code, 302)
        self.assertRedirects(result, '/create2/1')
        self.assertEqual(Character.objects.count(), 1)

    def test_create2(self):
        cl = Client()

        result = cl.get('/create')
        self.assertRedirects(result, '/accounts/login?next=/create', status_code=302, target_status_code=301)

        cl.login(username=self.name, password=self.password)

        result = cl.get('/create')

        self.assertEqual(result.status_code, 200)

        post_data = {
            'name': 'yoo',
            'align': 1,
            'gender': 'test',
            'race': 1,
            'c_class': 1,
            'xp': 0,
            'str': 3,
            'dex': 5,
            'con': 8,
            'int': 4,
            'wis': 6,
            'cha': 4
        }

        result = cl.post('/create_char', post_data)
        self.assertEqual(result.status_code, 302)
        self.assertRedirects(result, '/create2/1')
        self.assertEqual(Character.objects.count(), 1)

        result = cl.post('/train/2/1')
        self.assertEqual(result.status_code, 403)
        self.assertEqual(Char_skills.objects.get(pk=1).ranks, 0)

        # no skill ranks
        result = cl.post('/train/1/2')
        self.assertEqual(result.status_code, 403)
        self.assertEqual(Char_skills.objects.get(pk=1).ranks, 0)

        char = Character.objects.get(pk=1)
        char.skill_ranks = 3
        char.save()

        result = cl.post('/train/1/1')
        self.assertEqual(result.status_code, 302)
        self.assertEqual(Char_skills.objects.get(pk=1).ranks, 1)

        result = cl.post('/train/1/1')
        self.assertEqual(result.status_code, 302)
        self.assertEqual(Char_skills.objects.get(pk=1).ranks, 1)

        char = Character.objects.get(pk=1)
        char.level = 2
        char.save()

        result = cl.post('/train/1/1')
        self.assertEqual(result.status_code, 302)
        self.assertEqual(Char_skills.objects.get(pk=1).ranks, 2)

