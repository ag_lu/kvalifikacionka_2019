# Generated by Django 2.2 on 2019-05-11 16:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CharacterGenerator', '0015_auto_20190511_1858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feat_prerequisites',
            name='atk_bonus',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='feat_prerequisites',
            name='atr_name',
            field=models.CharField(max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='feat_prerequisites',
            name='atr_quantity',
            field=models.IntegerField(default=13, null=True),
        ),
        migrations.AlterField(
            model_name='feat_prerequisites',
            name='cleric',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name='feat_prerequisites',
            name='feat',
            field=models.CharField(max_length=40, null=True),
        ),
        migrations.AlterField(
            model_name='feat_prerequisites',
            name='wizard',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
