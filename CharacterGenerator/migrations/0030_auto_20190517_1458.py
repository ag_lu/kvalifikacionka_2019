# Generated by Django 2.2 on 2019-05-17 11:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CharacterGenerator', '0029_auto_20190516_2028'),
    ]

    operations = [
        migrations.RenameField(
            model_name='character',
            old_name='gp',
            new_name='cp',
        ),
    ]
