from django.contrib import admin
from .models import Alignments, Race, Class, Character, Racial_traits, Skills, \
    Char_skills, Features, Feat_prerequisites, Char_features, Items, Gods, Schools, Equipment, Spells, Char_spells, \
    Special_spells, Char_class_feats, Class_feats, Adventures, Char_adventures

# Register your models here.
admin.site.register(Alignments)
admin.site.register(Race)
admin.site.register(Class)
admin.site.register(Character)
admin.site.register(Racial_traits)
admin.site.register(Skills)
admin.site.register(Char_skills)
admin.site.register(Features)
admin.site.register(Feat_prerequisites)
admin.site.register(Char_features)
admin.site.register(Items)
admin.site.register(Gods)
admin.site.register(Schools)
admin.site.register(Equipment)
admin.site.register(Spells)
admin.site.register(Char_spells)
admin.site.register(Special_spells)
admin.site.register(Char_class_feats)
admin.site.register(Class_feats)
admin.site.register(Adventures)
admin.site.register(Char_adventures)