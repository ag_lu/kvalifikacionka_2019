from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from random import randint


#TODO !!!!!! Set nulls, on_delete!!!!!
class Alignments(models.Model):
    code = models.CharField(max_length=4)
    name = models.CharField(max_length=20)
    desc = models.CharField(max_length=200)

    def __str__(self):
        return self.code + ' ' + self.name


class Race(models.Model):
    name = models.CharField(max_length=16)
    desc = models.TextField(max_length=1000)
    speed = models.IntegerField()
    short_desc = models.CharField(max_length=200)
    ranks = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Racial_traits(models.Model):
    race_id = models.ForeignKey(Race, on_delete=models.PROTECT)
    name = models.CharField(max_length=40)
    desc = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Class(models.Model):
    name = models.CharField(max_length=25)
    desc = models.CharField(max_length=200)
    hp = models.IntegerField()
    hit_dice = models.IntegerField()
    fort_save = models.IntegerField()
    ref_save = models.IntegerField()
    will_save = models.IntegerField()
    atk_bonus = models.IntegerField()
    skill_rank = models.IntegerField()
    cp = models.IntegerField(default=0)
    max_spells = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return self.name + ' ' + str(self.hp)

    def hp_dice(self):
        return randint(1, self.hit_dice)


class Gods(models.Model):
    name = models.CharField(max_length=30)
    desc = models.CharField(max_length=300)
    holy_weapon = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Schools(models.Model):
    name = models.CharField(max_length=40)
    desc = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Character(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    hp = models.IntegerField(default=0)
    alignment = models.ForeignKey(Alignments, on_delete=models.DO_NOTHING)
    gender = models.CharField(max_length=50)
    race = models.ForeignKey(Race, on_delete=models.DO_NOTHING)
    c_class = models.ForeignKey(Class, on_delete=models.DO_NOTHING)
    school = models.ForeignKey(Schools, on_delete=models.SET_NULL, blank=True, null=True, default=None)
    god = models.ForeignKey(Gods, on_delete=models.SET_NULL, blank=True, null=True, default=None)

    level = models.IntegerField(default=1)
    xp = models.IntegerField(default=0)

    cp = models.IntegerField(default=0)

    avb_feats = models.IntegerField(default=2)
    skill_ranks = models.IntegerField(default=2)

    atk_bonus = models.IntegerField(default=0)
    fort_save = models.IntegerField(default=0)
    ref_save = models.IntegerField(default=0)
    will_save = models.IntegerField(default=0)

    max_spells = models.IntegerField(default=0, null=True, blank=True)

    strength = models.IntegerField(default=0)
    dexterity = models.IntegerField(default=0)
    constitution = models.IntegerField(default=0)
    intelligence = models.IntegerField(default=0)
    wisdom = models.IntegerField(default=0)
    charisma = models.IntegerField(default=0)

    def STR(self):
        return (self.strength - 10) // 2

    def DEX(self):
        return (self.dexterity - 10) // 2

    def CON(self):
        return (self.constitution - 10) // 2

    def INT(self):
        return (self.intelligence - 10) // 2

    def WIS(self):
        return (self.wisdom - 10) // 2

    def CHA(self):
        return (self.charisma - 10) // 2

    def __str__(self):
        return self.name + ' (' + self.c_class.name + ')'

    def can_reroll(self):
        if max([self.strength, self.dexterity, self.constitution,
                self.intelligence, self.wisdom, self.charisma]) <= 13:
            return True
        if sum([self.STR(), self.DEX(), self.CON(),
                self.INT(), self.WIS(), self.CHA()]) <= 3:
            return True

    def get_ranks(self):
        return self.c_class.skill_rank + (self.INT() if self.INT() >= 0 else 0) + self.race.ranks

    def skill_mods(self, mstr):
        if mstr == "DEX":
            return self.DEX()
        elif mstr == "CHA":
            return self.CHA()
        elif mstr == "STR":
            return self.STR()
        elif mstr == "WIS":
            return self.WIS()
        elif mstr == "INT":
            return self.INT()
        else:
            return self.CON()

    def get_atr(self, astr):
        if astr == "Dexterity":
            return self.dexterity
        elif astr == "Charisma":
            return self.charisma
        elif astr == "Strength":
            return self.strength
        elif astr == "Wisdom":
            return self.wisdom
        elif astr == "Intelligence":
            return self.intelligence
        else:
            return self.constitution


class Spells(models.Model):
    name = models.CharField(max_length=40)
    wizard = models.BooleanField(default=False)
    cleric = models.BooleanField(default=False)
    desc = models.CharField(max_length=400)
    range = models.IntegerField(default=0)
    duration = models.IntegerField(default=0)
    cantrip = models.BooleanField(default=False)
    level = models.IntegerField(default=1)
    uses = models.IntegerField(default=0, null=True, blank=True)
    restricted_i = models.BooleanField(default=False)
    restricted_e = models.BooleanField(default=False)
    mod = models.CharField(max_length=15, null=True, blank=True)
    unique = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Special_spells(models.Model):
    god_id = models.ForeignKey(Gods, on_delete=models.CASCADE, null=True, blank=True)
    school_id = models.ForeignKey(Schools, on_delete=models.CASCADE, null=True, blank=True)
    spell_id = models.ForeignKey(Spells, on_delete=models.CASCADE)


class Char_spells(models.Model):
    char_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    spell_id = models.ForeignKey(Spells, on_delete=models.PROTECT)
    uses = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return self.char_id.name + ' ' + self.spell_id.name


class Skills(models.Model):
    modifier = models.CharField(max_length=5)
    name = models.CharField(max_length=30)
    desc = models.CharField(max_length=250)
    trained = models.BooleanField(default=False)
    fighter = models.BooleanField(default=False)
    cleric = models.BooleanField(default=False)
    rogue = models.BooleanField(default=False)
    wizard = models.BooleanField(default=False)

    def __str__(self):
        return self.name + '(' + self.modifier + ')'


class Char_skills(models.Model):
    char_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    skill_id = models.ForeignKey(Skills, on_delete=models.PROTECT)
    ranks = models.IntegerField(default=0)
    misc = models.IntegerField(default=0)
    is_class = models.BooleanField(default=False)


class Items(models.Model):
    name = models.CharField(max_length=30)
    price = models.IntegerField(default=0)
    a_type = models.CharField(max_length=10, blank=True, null=True)
    a_class = models.IntegerField(blank=True, null=True)
    hands = models.CharField(max_length=10, blank=True, null=True)
    w_range = models.CharField(max_length=10, blank=True, null=True)
    w_type = models.CharField(max_length=15, blank=True, null=True)
    ammo = models.IntegerField(blank=True, null=True)
    damage = models.CharField(max_length=10, blank=True, null=True)
    crit = models.IntegerField(blank=True, null=True, default=20)
    desc = models.CharField(max_length=200, blank=True, null=True)
    masterwork = models.BooleanField(default=False)
    magical = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return self.name + ' ' + str(self.price)

    def is_weapon(self):
        return self.w_range is not None or self.w_type is not None

    def is_armor(self):
        return self.a_type is not None or self.a_class is not None


class Features(models.Model):
    name = models.CharField(max_length=40)
    benefit = models.CharField(max_length=500)
    power_up = models.CharField(max_length=500, null=True, default=None)
    penalty = models.CharField(max_length=500, null=True, default=None)
    multiple = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_req(self):
        try:
            req = Feat_prerequisites.objects.get(feat_id=self)
        except ObjectDoesNotExist:
            return None

        return req.better_str()


class Char_features(models.Model):
    char_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    feat_id = models.ForeignKey(Features, on_delete=models.DO_NOTHING)
    skill_id = models.ForeignKey(Skills, on_delete=models.DO_NOTHING, null=True, default=None, blank=True)
    weapon_id = models.ForeignKey(Items, on_delete=models.DO_NOTHING, null=True, default=None, blank=True)
    spell_id = models.ForeignKey(Spells, on_delete=models.DO_NOTHING, null=True, default=None, blank=True)


class Feat_prerequisites(models.Model):
    feat_id = models.ForeignKey(Features, on_delete=models.DO_NOTHING, related_name='feat')
    atr_name = models.CharField(max_length=15, blank=True, null=True)
    atr_quantity = models.IntegerField(default=13, null=True, blank=True)
    req_feat = models.ForeignKey(Features, on_delete=models.DO_NOTHING, related_name='req_feat', null=True, default=None, blank=True)
    wizard = models.BooleanField(default=False, null=True)
    cleric = models.BooleanField(default=False, null=True)
    atk_bonus = models.IntegerField(default=0, null=True)
    weapon = models.ForeignKey(Items, on_delete=models.DO_NOTHING, default=None, null=True, blank=True)

    def __str__(self):
        res = self.feat_id.name

        if self.atr_name is not None:
            res += ' ' + str(self.atr_name) + ' ' + str(self.atr_quantity)
        if self.req_feat is not None:
            res += ' ' + self.req_feat.name
        if self.wizard:
            res += ' WIZARD'
        if self.cleric:
            res += ' CLERIC'

        return res

    def better_str(self):
        res = ''

        if self.atr_name is not None:
            res += str(self.atr_name) + ': ' + str(self.atr_quantity)
        if self.req_feat is not None:
            if res != "":
                res += ', '
            res += 'Required feature: ' + self.req_feat.name
        if self.wizard:
            if res != "":
                res += ', '
            res += 'Character is wizard'
        if self.cleric:
            if self.wizard:
                res += '/ '
            elif res != "":
                res += ', '
            res += 'Character is cleric'
        if self.atk_bonus != 0:
            if res != "":
                res += ', '
            res += 'Attack bonus: ' + str(self.atk_bonus)

        return res

    
class Equipment(models.Model):
    char_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    item_id = models.ForeignKey(Items, on_delete=models.SET_NULL, blank=True, null=True)
    amount = models.IntegerField(default=0, null=True)

    def __str__(self):
        return self.char_id.name + ' ' + self.item_id.name + ' ' + str(self.amount if self.amount > 1 else '')


class Class_feats(models.Model):
    class_id = models.ForeignKey(Class, on_delete=models.PROTECT)
    name = models.CharField(max_length=50)
    desc = models.CharField(max_length=400, null=True, blank=True)
    level = models.IntegerField(default=1, null=True, blank=True)
    can_choose = models.BooleanField(default=False)


class Char_class_feats(models.Model):
    char_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    c_feat_id = models.ForeignKey(Class_feats, on_delete=models.CASCADE)

    def __str__(self):
        return self.c_feat_id.name


class Adventures(models.Model):
    name = models.CharField(max_length=40)
    desc = models.TextField(null=True, blank=True)


class Char_adventures(models.Model):
    char_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    adventure = models.ForeignKey(Adventures, on_delete=models.CASCADE)
    notes = models.TextField(null=True, blank=True)
    kills = models.IntegerField(default=0)
    xp = models.IntegerField(default=0)






