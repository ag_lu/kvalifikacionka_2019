from django import forms
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from CharacterGenerator import const
from CharacterGenerator.models import Race, Alignments, Class, Gods, Schools, Items


def minmax(val):
    if val > 20 or val < 3:
        raise ValidationError('Incorrect value')


class CreationForm(forms.Form):
    name = forms.CharField(max_length=50)
    gender = forms.CharField(max_length=50)
    race = forms.IntegerField()
    c_class = forms.IntegerField()
    align = forms.IntegerField()
    xp = forms.IntegerField()
    school = forms.IntegerField(required=False)
    mwp = forms.IntegerField(required=False)
    god = forms.IntegerField(required=False)
    str = forms.IntegerField(validators=[minmax])
    dex = forms.IntegerField(validators=[minmax])
    con = forms.IntegerField(validators=[minmax])
    int = forms.IntegerField(validators=[minmax])
    wis = forms.IntegerField(validators=[minmax])
    cha = forms.IntegerField(validators=[minmax])

    def clean_align(self):
        align = self.cleaned_data["align"]

        try:
            tmp = Alignments.objects.get(pk=align)
        except ObjectDoesNotExist:
            raise ValidationError(const.err_nexist.format("Alignment"))

        return align

    def clean_c_class(self):
        c_class = self.cleaned_data["c_class"]

        try:
            tmp = Class.objects.get(pk=c_class)
        except ObjectDoesNotExist:
            raise ValidationError(const.err_nexist.format("Class"))

        return c_class

    def clean_race(self):
        race = self.cleaned_data["race"]

        try:
            tmp = Race.objects.get(pk=race)
        except ObjectDoesNotExist:
            raise ValidationError(const.err_nexist.format("Race"))

        return race

    def clean_god(self):
        god = self.cleaned_data["god"]

        if god is not None:
            try:
                tmp = Gods.objects.get(pk=god)
            except ObjectDoesNotExist:
                raise ValidationError(const.err_nexist.format("God"))

        return god

    def clean_school(self):
        school = self.cleaned_data["school"]

        if school is not None:
            try:
                tmp = Schools.objects.get(pk=school)
            except ObjectDoesNotExist:
                raise ValidationError(const.err_nexist.format("School"))

        return school

    def clean_mwp(self):
        mwp = self.cleaned_data["mwp"]

        if mwp is not None:
            try:
                tmp = Items.objects.get(pk=mwp)
            except ObjectDoesNotExist:
                raise ValidationError(const.err_nexist.format("Weapon"))

        return mwp

    def clean(self):
        super().clean()

        if "c_class" not in self.cleaned_data or self.cleaned_data["c_class"] is None:
            # self.add_error("c_class", "Class required")
            return

        c_class = Class.objects.get(pk=self.cleaned_data["c_class"])

        if c_class.name == const.wizard:
            if self.cleaned_data["school"] is None:
                self.add_error("school", "School required")
                #raise ValidationError("School required")
            if self.cleaned_data["mwp"] is None:
                self.add_error("mwp", "Weapon required")
                #raise ValidationError("Weapon required")

        if c_class.name == const.cleric:
            if self.cleaned_data["god"] is None:
                self.add_error("god", "God required")
                #raise ValidationError("God required")


# sign_up form https://simpleisbetterthancomplex.com/tutorial/2017/02/18/how-to-create-user-sign-up-view.html#sign-up-with-extra-fields
class Sign_up(UserCreationForm):
    first_name = forms.CharField(max_length=30,  help_text='Required')
    last_name = forms.CharField(max_length=30, help_text='Required')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')


class Item_form(forms.Form):
    name = forms.CharField(max_length=30)
    desc = forms.CharField(max_length=200)
    price = forms.IntegerField


class Account_form(forms.Form):
    fname = forms.CharField(max_length=30)
    lname = forms.CharField(max_length=150)


class Edit_form(forms.Form):
    name = forms.CharField(max_length=50, required=False)
    xp = forms.IntegerField(required=False, min_value=0)
    cp = forms.IntegerField(required=False, min_value=0)
